<%-- 
    Document   : home
    Created on : 05 5, 18, 10:30:13 AM
    Author     : Mark
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../stylesheets/stylesheet.css">
        <link href="../stylesheets/bootstrap.css" rel="stylesheet" />
        <link href="../stylesheets/style.css" rel="stylesheet"/>
        <title>Home</title>
    </head>
    <body>
        <div style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.7)), url(../Images/bokeh_night_city_view_lights.jpg); background-attachment: fixed; background-size: cover;">
                <jsp:include page="header.jsp"/>
                <div class="main-banner pt64 pb64" id="home">
                    <div class="container pt64 pb64">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="text-white text-center">
                                    <div class="font-64">Let us help<br><br/></div>
                                </div>
                            </div>
                        </div>
                        <div class="home-service clearfix">
                            <div class="wrapper-demo1">
                                <div id="dd1" class="wrapper-dropdown-3" tabindex="1">
                                <span class="active">Select City</span>
                                <ul class="dropdown">
                                <li><a href="javascript:;" class="icon-menu " data-id="2" data-value="Quezon City">Quezon City</a></li>
                                <li><a href="javascript:;" class="icon-menu " data-id="2" data-value="Pasig">Pasig</a></li>

                                </ul>
                                </div>
                            </div>
                            <div class="wrapper-demo">
                                <div id="dd" class="wrapper-dropdown-2" tabindex="1"><span>Select a Service</span>
                                    <ul class="dropdown">
                                    <li><a href="javascript:;" class="icon-2 icon-menu" data-id="2" data-value="Gown">Gown</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="projects bg-white">
                <div class="container pb40">
                    <div class="row pt40">
                        <div class="col-xs-12">
                            <div class="font-21 text-center text-700 text-copy-1 uppercase text-space-wide">Needs for you</div>
                        </div>
                    </div>
                    <div class="row pt32">
                        <div class="col-sm-3 col-xs-12">
                            <div class="project-tile mb32" style="background-image: url(&#39;../Images/Customer profile pic.jpg&#39;)">
                                <div class="project-tile-text p24">
                                    <div class="font-16 uppercase text-700">Park Hye-min<br /><br /></div>
                                    <div class="font-14 pt32">Service needed:<br />Gown<br /></div>
                                    <div class="btn btn-sm btn-white-clear mt16 hidden">Read More   </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="project-tile mb32" style="background-image: url(&#39;../Images/aragaki yui aaaaaa.jpg&#39;)">
                                <div class="project-tile-text p24">
                                    <div class="font-16 uppercase text-700">Aragaki Yui<br /><br /></div>
                                    <div class="font-14 pt32">Service needed:<br />Gown<br /></div>
                                    <div class="btn btn-sm btn-white-clear mt16 hidden">Read More  </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="project-tile mb32" style="background-image: url(&#39;../Images/DHk8uHmUMAI0Z-f.jpg&#39;)">
                                <div class="project-tile-text p24">
                                    <div class="font-16 uppercase text-700">Francine Canosa<br /><br /></div>
                                    <div class="font-14 pt32">Service:<br />Gown<br /></div>
                                    <div class="btn btn-sm btn-white-clear mt16 hidden">Read More</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="project-tile mb32" style="background-image: url(&#39;../Images/suzy.jpg&#39;)">
                                <div class="project-tile-text p24">
                                    <div class="font-16 uppercase text-700">Suzy Bae<br /><br /></div>
                                    <div class="font-14 pt32">Service:<br />Gown<br /></div>
                                    <div class="btn btn-sm btn-white-clear mt16 hidden">Read More</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-black border-top border-bottom" id="contact">
                <div class="container">
                    <div class="row pt24 pb24">
                        <div class="col-xs-12">
                            <img class="img-max-w-200" src="../Images/merry-match-white.png" />
                        </div>
                    </div>
                    <div class="row pt24 pb24 font-14 text-white">
                        <div class="col-xs-12 col-sm-3">Your online marketplace<br />for wedding needs.
                        </div>
                        <div class="col-xs-12 col-sm-3">merrymatch@gmail.com<br />452 9113</div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="text-right"><br />Copyright 2018. Merry Match, Inc.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
    <script>
        $(document).ready(function(){
            $(".wrapper-dropdown-3").on("click",function(){
                $(this).toggleClass("active")
            })
            $(".wrapper-dropdown-2").on("click",function(){
                $(this).toggleClass("active")
            })
        })
    </script>
</html>
