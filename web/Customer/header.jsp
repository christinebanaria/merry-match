<%-- 
    Document   : header
    Created on : 05 6, 18, 10:48:35 PM
    Author     : Mark
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <nav class="navbar navbar-relyable mb0">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="container-fluid"></div>
                            <div class="navbar-header">
                                <button aria-expanded="false" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span><span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="home.jsp">
                                <img src="../Images/merry-match-white.png" /></a>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav pull-right">
                                <li class="active"><a class="uppercase text-700" href="#home">Home</a></li>
                                <li><a class="uppercase text-700" href="viewProfile.jsp">Profile</a></li>
                                <li><a class="uppercase text-700" href="chat.jsp">Chat</a></li>
                                <li><a class="uppercase text-700" href="#contact">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </body>
</html>
