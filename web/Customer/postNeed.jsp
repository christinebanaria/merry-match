<%-- 
    Document   : postNeed
    Created on : 05 5, 18, 11:01:52 AM
    Author     : Mark
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../stylesheets/bootstrap.css" rel="stylesheet" />
        <link href="../stylesheets/style.css" rel="stylesheet"/>
        <title>Post your need</title>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <h1>Post a need</h1>
        
        <select class="dropdown-filter" name="filter year">
            <option selected disabled>Select service</option>
            <option value="Attire">Gown</option>
            <option value="Catering">Catering</option>
            <option value="Choir">Choir</option>
            <option value="Confectionery">Confectionery</option>
            <option value="Entertainment">Entertainment</option>
            <option value="Facilities">Facilities</option>
            <option value="Flowers">Flowers</option>
            <option value="Hair">Hair</option>
            <option value="Invitation Designing">Invitation Designing</option>
            <option value="Invitation Printing">Invitation Printing</option>
            <option value="Makeup">Makeup</option>
            <option value="Photobooth">Photobooth</option>
            <option value="Photography">Photography</option>
            <option value="Sound">Sound</option>
            <option value="Transportation">Transportation</option>
            <option value="Venue">Venue</option>
            <option value="Venue Designer">Venue Designer</option>
            <option value="Videography">Videography</option>
            <option value="Wedding Souvenir">Wedding Souvenir</option>
            
        </select><br>
        <textarea name="Text1" cols="50" rows="10" value="">Date: February 10, 2019
Looking for gowns</textarea><br>
        <a href="viewProfile.jsp"><button name="saveNeed">Save</button></a>
    </body>
</html>
