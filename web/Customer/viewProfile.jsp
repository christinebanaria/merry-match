<%-- 
    Document   : viewProfile
    Created on : 05 5, 18, 9:27:08 AM
    Author     : Erica Ang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../stylesheets/stylesheet.css">
        <link href="../stylesheets/bootstrap.css" rel="stylesheet" />
        <link href="../stylesheets/style.css" rel="stylesheet"/>
        <title>View Customer Profile</title>
    </head>
    <body>
        <nav class="navbar navbar-relyable mb8">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="container-fluid"></div>
                            <div class="navbar-header">
                                <button aria-expanded="false" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span><span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#">
                                <img src="../Images/merry-match-white.png" /></a>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav pull-right">
                                <li class="active"><a class="uppercase text-700" href="home.jsp">Home</a></li>
                                <li><a class="uppercase text-700" href="viewProfile.jsp">Profile</a></li>
                                <li><a class="uppercase text-700" href="#products-and-services">Chat</a></li>
                                <li><a class="uppercase text-700" href="#contact">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        
        <div style="position: absolute; left: 27%; top: 100px">
            <div>
                <image src ="../Images/Customer profile pic.jpg" class="dp">
                <div class="user-details">
                    <b style="font-size: 28px">Park Hye-Min</b>
                    <div style="font-size: 18px">
                        ponypony <br>
                        0926 111 5342 | pony@ponysyndrome.com <br>
                        Manila, NCR

                    </div>
                    <br><br>
                </div>
                <div style="position: relative; margin-top: 45px">
                    <div style="font-size: 40px; text-align: center; border-bottom: 3px solid black; width: 680px; font-weight: bold; position: relative; left: -55px">NEEDS</div>
                </div>
                <div id="message"  style="font-size: 20px; text-align: center; width: 680px; position: relative; left: -55px; top: 5px"></div>
                <div style="position: absolute; margin-top: 20px">
                    <div>
                        <div style="position: absolute; margin-left: -70px">
                            <div class="open-brace">{</div>
                            <div class="profile-need" onclick="location.href='../Customer/need1Details.jsp'">Hair Stylist</div>
                            <div class="close-brace">}</div>
                        </div>
                        <div style="position: absolute; margin-left: 170px">
                            <div class="open-brace">{</div>
                            <div class="profile-need" onclick="need()">Catering</div>
                            <div class="close-brace">}</div>
                        </div>
                        <div style="position: absolute; margin-left: 410px">
                            <div class="open-brace">{</div>
                            <div class="profile-need" onclick="location.href='../Customer/need3Details.jsp'">Gowns</div>
                            <div class="close-brace">}</div>
                        </div>
                    </div>

                    <div style="position: absolute; margin-top: 120px">
                        <div style="position: absolute; margin-left: -70px">
                            <div class="open-brace">{</div>
                            <div class="profile-need" onclick="location.href='../Customer/need4Details.jsp'">Photography</div>
                            <div class="close-brace">}</div>
                        </div>
                        <div style="position: absolute; margin-left: 170px">
                            <div class="open-brace">{</div>
                            <div class="profile-need" onclick="location.href='../Customer/need5Details.jsp'">Flowers</div>
                            <div class="close-brace">}</div>
                        </div>
                        <div style="position: absolute; margin-left: 410px">
                            <div class="open-brace">{</div>
                            <div class="profile-need" onclick="location.href='../Customer/need6Details.jsp'">Makeup</div>
                            <div class="close-brace">}</div>
                        </div>    
                    </div>    
                </div>  
            </div>
        </div>
        
        
        
        
        
        


        
        <script>
        function need() {
            var txt;
            var message = prompt("Send quotations for:\n\n     Date: February 10, 2019\n     Looking for Japanese buffet good for 200 people", "");
            if (message == null || message == "") {
                txt = "";
            } else {
                txt = "Quotation for catering sent!";
            }
            document.getElementById("message").innerHTML = txt;
        }
        </script>
    </body>
</html>